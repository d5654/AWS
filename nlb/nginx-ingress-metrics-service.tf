resource "kubernetes_service" "nginx-ingress-metrics-service" {
  metadata {
    name      = "nginx-ingress-metrics-service"
    namespace = kubernetes_namespace.nginx.metadata.0.name
    labels = {
      "app.kubernetes.io/name"    = "ingress-nginx"
      "app.kubernetes.io/part-of" = "ingress-nginx"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].annotations,
      metadata[0].labels,
    ]
  }
  spec {

    type = "ClusterIP"
    selector = {
      "app.kubernetes.io/name"    = "ingress-nginx"
      "app.kubernetes.io/part-of" = "ingress-nginx"
    }
    port {
      name        = "metrics"
      port        = 10254
      target_port = 10254
    }

  }
}