output "loadbalancer-ip" {
    value = resource.kubernetes_service.nlb.status[*].load_balancer[*].ingress[*].hostname
}