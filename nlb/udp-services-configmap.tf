resource "kubernetes_config_map" "udp" {
  metadata {
    name      = "udp-services"
    namespace = kubernetes_namespace.nginx.metadata.0.name
    labels = {
      "app.kubernetes.io/name"    = "ingress-nginx"
      "app.kubernetes.io/part-of" = "ingress-nginx"
    }
  }

  lifecycle {
    ignore_changes = [
      metadata[0].annotations,
      metadata[0].labels,
    ]
  }
}