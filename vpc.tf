// VPC do eks
// Maiores informações em https://github.com/terraform-aws-modules/terraform-aws-vpc

locals {
  tags = merge(var.tags, { "Env" = var.ENVIRONMENT, "Terraform" = "true" })
}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "3.11.0"

  name                = format("vpc-%s", var.ENVIRONMENT)
  cidr                = var.cidr
  azs                 = var.azs
  private_subnets     = var.priv_subnets
  public_subnets      = var.pub_subnets
  single_nat_gateway  = true
  reuse_nat_ips       = true
  external_nat_ip_ids = [aws_eip.lb.allocation_id]

  propagate_public_route_tables_vgw = true

  enable_nat_gateway   = true
  enable_vpn_gateway   = true
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = merge(
    {
      format("kubernetes.io/cluster/eks-cluster-%s", var.ENVIRONMENT) = "shared"
    },
    local.tags,
  )

  public_subnet_tags = merge(
    {
      "kubernetes.io/role/elb"                                        = "1"
      format("kubernetes.io/cluster/eks-cluster-%s", var.ENVIRONMENT) = "shared"
    },
    local.tags,
  )

  private_subnet_tags = merge(
    {
      "kubernetes.io/role/internal-elb" = "1"
    },
    local.tags,
  )

  public_route_table_tags = merge(
    {
      "kubernetes.io/role/elb" = "1"
    },
    local.tags,
  )

  private_route_table_tags = merge( 
    {
      "kubernetes.io/role/internal-elb" = "1"
    },
    local.tags,
  )
}
