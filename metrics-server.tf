resource "helm_release" "metrics-server" {
  name = "metrics-server"

  chart     = "metrics-server"
  namespace = "kube-system"

  depends_on = [
    module.eks,
  ]
}