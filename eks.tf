// Criação do cluster
// Maiores informações em https://github.com/terraform-aws-modules/terraform-aws-eks
module "eks" {
  source           = "terraform-aws-modules/eks/aws"
  version          = "~> 17.23.0"
  cluster_name     = format("eks-cluster-%s", var.ENVIRONMENT)
  cluster_version  = var.cluster_version
  subnets          = module.vpc.private_subnets
  write_kubeconfig = "false"
  vpc_id           = module.vpc.vpc_id
  enable_irsa      = true

  node_groups_defaults = {
    subnets   = module.vpc.private_subnets
    ami_type  = "AL2_x86_64"
    disk_size = 30
  }

  node_groups = var.node_pools

  tags = local.tags
}