### Global ###
variable "ENVIRONMENT" {
  type = string
}

variable "aws_region" {
  description = "AWS region. Reference: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-available-regions"
  type = string
}

variable "tags" {
  description = "A mapping of tags to assign to the resource"
  type        = map(string)
  default = {
    Owner   = "Nucleo-DevOps"
    Project = "Webinar"
    CC      = "Aubay-Portugal"
  }
}


### Key pair ###
variable "key_name" {
  description = "Key pair RSA name."
  type = string
}

variable "public_key" {
  type = string
}


### EKS ###
variable "cluster_version" {
  type = string
}

variable "node_pools" {
  type = map(any)
}


### VPC ###
variable "cidr" {
  description = "The CIDR block for the VPC. Default value is a valid CIDR, but not acceptable by AWS and should be overridden"
  type = string
}

variable "azs" {
  description = "A list of availability zones names or ids in the region"
  type = list(string)
}

variable "priv_subnets" {
  description = "A list of private subnets inside the VPC"
  type = list(string)
}

variable "pub_subnets" {
  description = "A list of public subnets inside the VPC"
  type = list(string)
}
