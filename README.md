# Workshop AWS Kubernetes

## Preparation
- It's necessary to have a S3 bucket previously created to store the Terraform state
- The account in use must have the following permissions on the bucket, according to Terraform [documentation](https://www.terraform.io/docs/language/settings/backends/s3.html#s3-bucket-permissions):
    - s3:ListBucket
    - s3:GetObject
    - s3:PutObject
- It's also expected that the account used for deploy has permission to create the resources (VPC, EKS, ElasticIP)
- The following variables must be set (Project page > Settings > CI/CD Variables) into the Gitlab:
  - AWS_DEFAULT_REGION
  - AWS_ACCESS_KEY_ID
  - AWS_SECRET_ACCESS_KEY
  - S3_BUCKET

## Cluster creation
- All the variables must be set inside a file on [envs](envs) folder, as example of [env1.tfvars](envs/env1.tfvars). All the values must be set, as the code doesn't have any default or treatment for empty values other than Terraform ones.
- For each new environmet, it's necessary to replicate the pipeline file and customise it's value,as example of 'ENV1' on [.gitlab-ci/env1.yaml](.gitlab-ci/env1.yaml). It's also necessary to add a line to "import" the new pipeline into the [.gitlab-ci.yml](.gitlab-ci.yml).
- This example includes an NGINX Ingress deploy on [nlb](nlb) folder and the [metrics-server](metrics-server) deploy for the cluster.
- After variables are set, run the pipeline on Gitlab-CI, while setting the "ENV" variable, as follows:  
  ![](doc/set-var.png)

## Cluster Access
- To fetch credentials and access the cluster, you can follow [this](https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html
) AWS guide.

## Cleanup
- After using the environment, just run the pipeline on Gitlab-CI, set the "ENV" and a variable called "DESTROY" to "true" and it will erase everything.

## Observations
- Sometimes the Terraform fails right before cluster creation is complete. It's a known problem with the module. Running the pipeline again usually works and the apply step gets completed.