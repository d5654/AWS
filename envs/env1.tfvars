### Geral ###

aws_region = "us-east-1"

### Key pair ###
key_name = "webinar-rsa"
public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDd/q9jt0+Mw7JL5RjDl5kb5/mrHh4F21eEuYurDdts/8xmmYjbjfZ2juEfdvpLTnfbgFQ/BCJ2r/97eggMS9DiYeGVn48d4TGsnaqEu0sUaap5hdzblY6gJfdTg5gr7fyWJWqpZP5SHzPEnBOCCYHKHkRmjNmATcRpaPgNt8GWl2BvAXfv+Tkaun9Elsw0HuaQKzR/07pTVW7oR2rj43g078DLmi9tBlvugkt0WwbmKqbcAjzHgL+ukzUGtqdFaoFPB5OZe2onWk5olsswaD9r4yEEBSBSb6mfT2A4zpEAuJ4ApGiHiw4XQ+lHPdJrHnuhJfPnuVfLmyAaxleVF/wT webinar-nucleo-devops"

### EKS ###

cluster_version = "1.21"

node_pools = {
    nodepool_business_01 = {
      name             = "eks-nodepool-business-01"
      desired_capacity = 2
      max_capacity     = 5
      min_capacity     = 2
      key_name         = "webinar-rsa"
      instance_type    = "t3.medium"
      k8s_labels = {
        Terraform           = "true"
        propagate_at_launch = true
      }
    }
}

### VPC ###
cidr = "10.1.0.0/16"
azs = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d"]
priv_subnets = ["10.1.0.0/20", "10.1.16.0/20", "10.1.32.0/20", "10.1.48.0/20"]
pub_subnets = ["10.1.64.0/20", "10.1.80.0/20", "10.1.96.0/20", "10.1.112.0/20"]
